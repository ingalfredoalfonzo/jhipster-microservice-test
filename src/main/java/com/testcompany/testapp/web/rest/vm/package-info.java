/**
 * View Models used by Spring MVC REST controllers.
 */
package com.testcompany.testapp.web.rest.vm;
